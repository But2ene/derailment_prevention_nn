derailment_prevention_nn
==============================

A project to predict train derailment with neural networks


# Installation

- Install Anaconda
- Copy the project in a directory
  - With Graphical Interface, visit https://gitlab.com/But2ene/derailment_project_nn
  - From command line (Anaconda prompt on Windows, plain terminal in Mac & Linux)
```
git clone https://gitlab.com/But2ene/derailment_prevention_nn.git
```

- Create a virtual environment to install all the dependancies in
  Run this command on Anaconda prompt (Windows & Mac) or terminal (Linux)

```
# set the project directory as the current directory
cd /path/to/project/derailment_prevention_nn

make create_environment
```

- Activate the virtual environment

  Use either the Anaconda GUI or command line

  - With Graphical Interface (Recommended for Windows)
    - Open **Anaconda Navigator**
    - Go to **Environments**
    - Select derailment_prevention_nn

  - With Command Line (Anaconda prompt on Windows & Mac, terminal on linux)

```
source activate derailment_prevention_nn
```

- Install pytorch from https://pytorch.org/

> Pytorch must be installed before you run the next command!

- Install Jupyter and Spyder in the new virtual env
  - Either with the GUI
  - Or, if it doesn't work, with the command line

```
conda install jupyter conda
```

- Install dependencies (run this command in the project directory)
```
make requirements
```

- Download the environment file (it gives private info)

It is located on the private drive, it's name is `.env`.
Put it at the root of the project.


- Prepare the data

Download them and put them in the `data/raw` directory

Then run the command
```
make data
```

> This command copy the data into a small database for faster access
> See the example when `src.data.io` is imported

# QuickStart

- Activate the virtual environment

> See Installation


- Open Spyder/Jupyter 

You can now start to work :)


To stop working on it, you can change of virtual environment either by:
- selecting root environment with Anaconda Navigator
- if (only if) you activated the environment from command line, run

```
source deactivate
```


# How to use

From where should I run my code?
> To run experimentations and visualisations, the `notebook` directory.
> **Never run code from inside the `src` directory!**

How to use the code in the source directory `src`?
> Use this line
```
from src.my_module import my_sub_module
```








Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>





