from torch.utils import data as tdata
# -*- coding: utf-8 -*-
"""
try pytorch example
"""

import torch
import numpy as np

import src.data.io as io


df_vampire = io.get('raw/vampire/sf1')
df_track = io.get('raw/track/sf1')

num_elem = 5000

to_predict = ['Y/Q Left wheel WS1',
               'Y/Q Right wheel WS1',
               'Y/Q Left wheel WS2',
               'Y/Q Right wheel WS2']

to_train = ['Cross-Level Irregularity', 'Curvature Irregularity',
       'Lateral Irregularity', 'Vertical Irregularity', 'Gauge Variation']

x = torch.from_numpy(df_track[to_train].iloc[:num_elem].values)
y = torch.from_numpy(df_vampire[to_predict].iloc[:num_elem].values)

mydset = tdata.TensorDataset(x, y)

# https://am207.github.io/2018spring/wiki/ValidationSplits.html

# define our indices -- our dataset has 9 elements and we want a 8:4 split
num_train = len(mydset)
indices = list(range(num_train))
split = 1000

# Random, non-contiguous split
validation_idx = np.random.choice(indices, size=split, replace=False)
train_idx = list(set(indices) - set(validation_idx))

# Contiguous split
# train_idx, validation_idx = indices[split:], indices[:split]

## define our samplers -- we use a SubsetRandomSampler because it will return
## a random subset of the split defined by the given indices without replac
train_sampler = tdata.SubsetRandomSampler(train_idx)
validation_sampler = tdata.SubsetRandomSampler(validation_idx)

train_loader = tdata.DataLoader(mydset,
                batch_size=100, sampler=train_sampler)

validation_loader = tdata.DataLoader(mydset,
                batch_size=100, sampler=validation_sampler)


##############
#
# Prepare model
#
##############


size_list = [x.size() for x in train_loader.dataset.tensors]

D_in = size_list[0][1]
H = size_list[0][1]
D_out = size_list[1][1]

# set hyper parameters
learning_rate = 1e-4

# model
#model = torch.nn.Sequential(
#    torch.nn.Linear(D_in, D_in),
#    torch.nn.ReLU(),
#    torch.nn.Linear(D_in, D_out),
#    torch.nn.ReLU(),
#    torch.nn.Linear(D_out, D_out),
#)

seq_len = 5
batch = 1
inp_size = D_in
hid_size = D_out
n_lay = 1

model = torch.nn.Sequential(
    torch.nn.Linear(D_in, D_in),
    torch.nn.RNN(input_size=D_in,
                 hidden_size=hid_size,
                 num_layers=n_lay,
                 nonlinearity="relu"),
    torch.nn.Linear(hid_size, D_out),
)
model = model.double()

# loss function
loss_fn = torch.nn.MSELoss(reduction='sum')

# optimizer

# learning
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)


##############
#
# train model
#
##############

train_loss = []
test_loss = []

for epoch in range(10):

    epoch_train_loss = []
    for batch_index, (x, y) in enumerate(train_loader):

        y_pred = model(x)

        loss = loss_fn(y_pred, y)

        epoch_train_loss.append(loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    with torch.no_grad():
            # train callback
            train_loss.append(sum(epoch_train_loss) / len(epoch_train_loss))

            # test callback
            # test values on test samples
            test_l = []
            for test_x, test_y in validation_loader:
                test_l.append(loss_fn(model(test_x), test_y).item())
            test_loss.append(sum(test_l) / len(test_l))

            # print
            #print(batch_index, loss.item())

##############
#
# Show resullts
#
##############

import pandas as pd
import matplotlib.pyplot as plt

df_loss = pd.DataFrame({'train':train_loss, 'test':test_loss})


plt.figure(figsize=[6, 4])
plt.semilogy('train', data = df_loss)
plt.semilogy('test', data = df_loss, color='k')

plt.figure(figsize=[6, 4])
plt.semilogy('train', data = df_loss[300:])
plt.semilogy('test', data = df_loss[300:], color='k')




