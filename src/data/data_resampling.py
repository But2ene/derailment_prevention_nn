
from scipy.interpolate import interp1d

########
#
# compute
#
########
def resample(df_model, x_col_model, df_target, x_col_target):

    df_out = df_target[[x_col_target]].copy()

    for col in df_model.drop(x_col_model, axis=1).columns:
        f = interp1d(df_model[x_col_model],
                                 df_model[col])

        df_out[col] = df_out[x_col_target].transform(f)

    return df_out


########
#
# plot
#
########

if __name__ == "__main__":
    import matplotlib.pyplot as plt

    dd = load_vampyre_data()

    df_upsampled = resample(dd['track_sf1'], 'Distance', dd['vampyre_sf1'], 'Distance')

    din1 = dd['track_sf1']
    din2 = df_upsampled

    print(din1.columns)
    print(din2.columns)

    xlim = 3.

    plt.figure(figsize=[12, 8])
    for i, col in enumerate(din1.columns):
        plt.subplot(3, 2, i + 1)
        plt.scatter(x='Distance', y=col, data = din1[din1['Distance'] < xlim])
        plt.scatter(x='Distance', y=col, data = din2[din2['Distance'] < xlim], color="red", marker='+', linestyle="-")
        plt.xlabel(col)

    plt.tight_layout()
    plt.show()
