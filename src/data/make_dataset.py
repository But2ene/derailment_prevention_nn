# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os
import glob
import pandas as pd
import src.data.raw_data_utils as rdu
from src.data.data_resampling import resample



def remove_dataset(project_dir):
    """
    remove all datasets
    """
    filelist  = glob.glob(os.path.join(project_dir, "data", "interim"))
    filelist += glob.glob(os.path.join(project_dir, "data", "processed"))
    for f in filelist:
        os.remove(f)


def raw_to_interim():

    project_dir = Path(__file__).resolve().parents[2]
    project_dir = str(project_dir)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    vampire_filename_template = os.path.join(
            project_dir, "data", "raw",
            os.environ.get("RAW_VAMPIRE_FILENAME_TEMPLATE"),
            )
    track_filename = os.path.join(
            project_dir, "data", "raw",
            os.environ.get("RAW_TRACK_FILENAME"),
            )

    store_path = os.path.join(
            project_dir,
            os.environ.get("STORE_PATH"),
            )

    with pd.HDFStore(store_path) as store:

        track_raw = rdu.open_raw_track_data(track_filename)

        for scale_factor in [1, 2]:
            vampire_path = os.path.join(project_dir, "data", "raw",
                                        vampire_filename_template.format(scale_factor))

            vampire_df = rdu.open_raw_vampire_data(vampire_path)
            track_df = rdu.process_track_data_scale(track_raw, scale_factor)
            track_upsampled = resample(track_df, 'Distance', vampire_df, 'Distance')

            store.put('raw/vampire/sf{}'.format(scale_factor),
                      vampire_df,
                      format='t')

            store.put('raw/track/sf{}'.format(scale_factor),
                      track_upsampled,
                      format='t')

            store.put('raw/track_original/sf{}'.format(scale_factor),
                      track_df,
                      format='t')




@click.command()
#@click.argument('input_filepath', type=click.Path(exists=True))
#@click.argument('output_filepath', type=click.Path())
#def main(input_filepath, output_filepath):
def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    raw_to_interim()

    logger.info('raw data uploaded to interim repertory')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
