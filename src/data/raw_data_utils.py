"""
load vampire datasets from file

from vampyre_dataset import load_vampyre_data
dd = load_vampyre_data()
"""

import pandas as pd

def scale_factor_apply(track, scale_factor):
    """apply scale factor to track data"""
    if track.name == 'Distance':
        return track
    return track * scale_factor


def process_track_data_scale(track_raw, scale_factor):
    """
    apply scale_factor to irregularities in track data
    """
    return track_raw.apply(scale_factor_apply, args=(scale_factor,))


def open_raw_track_data(fpath):

    track_raw = pd.read_table(filepath_or_buffer=fpath, skiprows=2, sep='\s+', header = None)

    header_names = ["Distance", "Cross-Level Irregularity", "Curvature Irregularity",
    "Lateral Irregularity", "Vertical Irregularity", "Gauge Variation"]

    track_raw.columns = header_names
    return track_raw


def open_raw_vampire_data(fpath):
    return pd.read_csv(filepath_or_buffer=fpath, skiprows=1)
