# -*- coding: utf-8 -*-
"""
https://pandas-docs.github.io/pandas-docs-travis/io.html#io-hdf5
"""
import os.path
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import pandas as pd

def get_store_path():

    project_dir = Path(__file__).resolve().parents[2]
    project_dir = str(project_dir)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    store_path = os.path.join(
            project_dir,
            os.environ.get("STORE_PATH"),
            )

    return store_path

def get(*args, **kwargs):
    with pd.HDFStore(get_store_path()) as store:
        out = store.get(*args, **kwargs)

    return out


def append(*args, **kwargs):
    with pd.HDFStore(get_store_path()) as store:
        store.append(*args, **kwargs)


def put(*args, **kwargs):
    with pd.HDFStore(get_store_path()) as store:
        store.put(*args, **kwargs, format='t')


def keys():
    with pd.HDFStore(get_store_path()) as store:
        k = store.keys()
    return k